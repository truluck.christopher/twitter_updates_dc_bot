# c
import discord
from discord.ext import tasks, commands
from discord.commands import slash_command

import tweepy
import json
import datetime

with open('data.json', 'r') as data:
    latest_tweets = json.load(data)

class Twitter_updates(commands.Cog):
    def __init__(self, bot, tweepy_api):
        self.bot = bot
        self.api = tweepy_api
        
    @commands.Cog.listener()
    async def on_ready(self):
        print("Starting task.")
        self.announcment_channel = self.bot.get_channel(928875225708367972) 
        self.check_for_new_tweets.start()

    @tasks.loop(seconds = 60.0)
    async def check_for_new_tweets(self):
        for user in latest_tweets.keys():
            tweet = self.api.user_timeline(
                screen_name = user,
                include_rts = False,
                count = 1,
                exclude_replies = True
            )[0]

            if latest_tweets[user] != tweet.id:
                latest_tweets[user] = tweet.id

                await self.update_latest_tweets()
                await self.load_latest_tweets()
                
                await self.announcment_channel.send(f"Hey dorks! {user} just sent a tweet!\n\nhttps://twitter.com/twitter/statuses/{tweet.id}")
                return

    

    @slash_command(description = "add a user to check for tweets from.")
    async def add(self, ctx, user: str):        
        try:
            # validate user exist ( to stop more errors )
            self.api.get_user(screen_name = user)
            latest_tweets[user] = 0

            await self.update_latest_tweets()
            await self.load_latest_tweets()
            
            embed = discord.Embed(
                title = "User added",
                description = f":white_check_mark: User '{user}' added!",
                color = 0x00FF00,
                timestamp = datetime.datetime.utcnow()
            )

            await ctx.respond(embed = embed)

        except tweepy.errors.HTTPException:

            embed = discord.Embed(
                title = "Error",
                description = f":x: user '{user}' does not have a twitter account. (Check your spelling?)",
                color = 0xFF0000,
                timestamp = datetime.datetime.utcnow()
            )

            await ctx.respond(embed = embed)
        

    @slash_command(description = "remove a user to check for tweets from.")
    async def remove(self, ctx, user: str):
        try:
            del latest_tweets[user]
            await self.update_latest_tweets()
            await self.load_latest_tweets()
            
            embed = discord.Embed(
                title = "User removed",
                description = f":white_check_mark: User '{user}' removed!",
                color = 0x00FF00,
                timestamp = datetime.datetime.utcnow()
            )
            
            await ctx.respond(embed = embed)
        
        except KeyError as k:
            
            embed = discord.Embed(
                title = "Error",
                description = f":x: User '{user}' does not exist!",
                color = 0xFF0000,
                timestamp = datetime.datetime.utcnow()
            )

            await ctx.respond(embed = embed)
 
    async def load_latest_tweets(self):
        with open('data.json', 'r') as data:
            self.latest_tweets = json.load(data)

    async def update_latest_tweets(self):
        with open('data.json', 'w') as data:
            json.dump(latest_tweets, data)